//
//  LoaderView.swift
//  WholeApp
//
//  Created by dedeepya reddy salla on 07/08/23.
//

import SwiftUI

struct LoaderView: View {
    var scale: Double
    var body: some View {
        ProgressView()
             .scaleEffect(scale)
             .progressViewStyle(CircularProgressViewStyle())
    }
}

struct LoaderView_Previews: PreviewProvider {
    static var previews: some View {
        LoaderView(scale: 1.0)
    }
}
