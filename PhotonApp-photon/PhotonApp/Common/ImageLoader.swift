//
//  ImageLoader.swift
//  PhotonApp
//
//  Created by dedeepya reddy salla on 18/08/23.
//

import UIKit
import Combine


class ImageCacher {
    static var cache = NSCache<NSString, UIImage>()
}

class ImageLoader: ObservableObject {
    
    @Published var uiImage: UIImage?
    var subscriber: AnyCancellable?
    
    func loadImage(url: String)  {
        guard let url = URL(string: url) else {
            return
        }
        
        if let image = ImageCacher.cache.object(forKey: url.absoluteString as NSString) {
            uiImage = image
        }
        
        let pub = URLSession.shared.dataTaskPublisher(for: url)
       
        /*
         map it
         then save in cache
         then set to image
         */
        subscriber = pub.map( { UIImage(data: $0.data) })
                .replaceError(with: nil)
                .handleEvents(receiveOutput: { uiimage in
                    if let image = uiimage {
                        ImageCacher.cache.setObject(image, forKey: url.absoluteString as NSString)
                    }
                }).assign(to: \.uiImage, on: self)
    }
}
