//
//  CustomImageView.swift
//  PhotonApp
//
//  Created by dedeepya reddy salla on 18/08/23.
//

import SwiftUI

struct CustomImageView: View {
    @StateObject var loader = ImageLoader()
    
    init(url: String) {
        self.loader.loadImage(url: url)
    }
    
    var body: some View {
        if let image = loader.uiImage {
        
        } else {
            
        }
    }
}

//struct CustomImageView_Previews: PreviewProvider {
//    static var previews: some View {
//        CustomImageView()
//    }
//}
