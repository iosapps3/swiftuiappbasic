//
//  PhotonAppApp.swift
//  PhotonApp
//
//  Created by dedeepya reddy salla on 18/08/23.
//

import SwiftUI

@main
struct PhotonAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
