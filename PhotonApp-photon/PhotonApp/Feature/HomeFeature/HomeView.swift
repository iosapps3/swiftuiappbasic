//
//  HomeView.swift
//  PhotonApp
//
//  Created by dedeepya reddy salla on 19/08/23.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
        albumsListview(showingAlert: false)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
