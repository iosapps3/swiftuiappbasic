//
//  ContentView.swift
//  PhotonApp
//
//  Created by dedeepya reddy salla on 18/08/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HomeView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
/*
 
 list - title, thumbnailUrl(thumbnail)
 detail page - title, url 
 {
     "albumId": 1, --
     "id": 1,
     "title": "accusamus beatae ad facilis cum similique qui sunt", --
     "url": "https://via.placeholder.com/600/92c952",
     "thumbnailUrl": "https://via.placeholder.com/150/92c952" --
   }
 */
