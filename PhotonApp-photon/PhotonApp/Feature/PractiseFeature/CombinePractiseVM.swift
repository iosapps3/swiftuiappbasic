//
//  CombinePractiseVM.swift
//  PhotonApp
//
//  Created by dedeepya reddy salla on 19/08/23.
//

import Foundation
import Combine
import UIKit

class BreedListviewModel: ObservableObject {
    @Published var apiStatus: APIStatus = .APINotCalled
    var subscriber: AnyCancellable?
    
    //
    @Published private(set) var time = Date().timeIntervalSince1970
    //remove later
    func fetchBreeds() {
/*
 test how initializing subscriber and not initializing effects sink and two types of assign
 */
         Timer.publish(
                 every: 1,
                 on: .main,
                 in: .default
             )
             .autoconnect()
             .sink { date in
                 self.time = date.timeIntervalSince1970
             }
        
//        breedService.fetchBreeds().receive(on: DispatchQueue.main)
//            .assign(to: &$breedList)
        
       
    }

    
}
