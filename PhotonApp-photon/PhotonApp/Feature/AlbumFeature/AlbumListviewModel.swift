//
//  AlbumListviewModel.swift
//  PhotonApp
//
//  Created by dedeepya reddy salla on 18/08/23.
//

import Foundation
import Combine

class AlbumListviewModel: ObservableObject {
    @Published private(set) var albumList = [AlbumModel]()
    @Published var apiStatus: APIStatus = .APINotCalled
    var subscriber: AnyCancellable?

    func fetchAlbums(albumService: AlbumServiceProtocol = AlbumService()) {
        self.apiStatus = .APIInProgress
        subscriber = albumService.fetchAlbums().sink { [weak self] status in
                switch status {
                case .finished:
                    self?.apiStatus = APIStatus.APIReceived
                    print("finished-------")
                    break
                case .failure(let errorInfo):
                    print("failure-------", errorInfo)
                    self?.apiStatus = APIStatus.apiFailed
                    break
                }
        } receiveValue: { [weak self] albums in
            self?.albumList = albums
        }
    }
}
