//
//  AlbumModel.swift
//  PhotonApp
//
//  Created by dedeepya reddy salla on 18/08/23.
//

import Foundation

struct AlbumModel: Hashable, Decodable {
    let id: Int
    let title: String
    let url: String
    let thumbnailUrl: String
}
