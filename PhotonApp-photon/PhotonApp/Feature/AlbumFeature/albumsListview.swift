//
//  albumsListview.swift
//  PhotonApp
//
//  Created by dedeepya reddy salla on 18/08/23.
//

import SwiftUI

struct albumsListview: View {
    @StateObject var albumListViewmModel = AlbumListviewModel()
    @State var showingAlert: Bool
    
    var body: some View {
        ZStack {
            if albumListViewmModel.apiStatus == APIStatus.APIInProgress {
                LoaderView(scale: 10.0)
            } else if albumListViewmModel.apiStatus == APIStatus.apiFailed {
                showAlert(showingAlert: true)
            }else {
                List {
                    ForEach(albumListViewmModel.albumList, id: \.id) { album in
                        HStack {
                            if !album.thumbnailUrl.isEmpty {
                                AsynchImageViewer(imageUrlStr: album.thumbnailUrl)
                                         
                //                AsyncImage(url: URL(string: currentWeather.skyImageUrl)) { image in
                //                    image
                //                        .resizable()
                //                        .scaledToFit()
                //                        .frame(maxHeight: 40)
                //                        .border(.green)
                //                } placeholder: {
                //                    ProgressView()
                //                        .frame(maxHeight: 40)
                //                        .border(.green)
                //                }
                            } else {
                                Image(systemName: "message.circle")
                                    .frame(maxHeight: 40)
                                    .border(.green)
                            }
                            Text(album.title)
                        }
                    }
                }
            }
        }.onAppear {
            albumListViewmModel.fetchAlbums()
         }
       
    }
    
    func showAlert(showingAlert: Bool) -> some View {
        Text("")
            .alert(isPresented: $showingAlert) {
                Alert(title: Text("Important message"), message: Text("Wear sunscreen"), dismissButton: .default(Text("Got it!")))
            }
    }
}

//struct albumsListview_Previews: PreviewProvider {
//    static var previews: some View {
//        albumsListview()
//    }
//}
