//
//  EnpointProtocol.swift
//  PhotonApp
//
//  Created by dedeepya reddy salla on 18/08/23.
//

import Foundation

protocol EndpointProtocol {
    var url: String {get}
}
