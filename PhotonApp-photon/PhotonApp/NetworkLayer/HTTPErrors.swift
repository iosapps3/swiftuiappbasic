//
//  HTTPErrors.swift
//  PhotonApp
//
//  Created by dedeepya reddy salla on 18/08/23.
//

import Foundation

enum HTTPErrors: Error {
    case urlError
    case apiError
    case parseError
}
