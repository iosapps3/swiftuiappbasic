//
//  HTTPApiHandler.swift
//  PhotonApp
//
//  Created by dedeepya reddy salla on 18/08/23.
//

import Foundation
import Combine

protocol HTTPApiHandlerProtocol {
    func fetchData<T:Decodable>(router: EndpointProtocol, dataType: T.Type) -> AnyPublisher<T, Error>
}

struct HTTPApiHandler: HTTPApiHandlerProtocol {
    
    
    func fetchData<T:Decodable>(router: EndpointProtocol, dataType: T.Type) -> AnyPublisher<T, Error> {
        
        guard let url = URL(string: router.url) else {
            return Fail(error: HTTPErrors.urlError).eraseToAnyPublisher()
        }
        
        let pub = URLSession.shared.dataTaskPublisher(for: url)
        let dataPublisher = pub.tryMap { (data, response) in
            if let resp = response as? HTTPURLResponse, resp.statusCode == 200 {
                return data
            }
            
            throw HTTPErrors.apiError
        }
        
        let parsedDataPub = dataPublisher.decode(type: T.self, decoder: JSONDecoder()).receive(on: DispatchQueue.main).eraseToAnyPublisher()
        return parsedDataPub
    }
}




