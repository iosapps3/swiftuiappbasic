//
//  AlbumEndpoint.swift
//  PhotonApp
//
//  Created by dedeepya reddy salla on 18/08/23.
//

import Foundation

enum AlbumEndpoint: EndpointProtocol {
    case albums

    var url: String {
        switch self {
        case .albums:
            return "https://jsonplaceholder.typicode.com/photos"
        }
    }
}
