//
//  AlbumService.swift
//  PhotonApp
//
//  Created by dedeepya reddy salla on 18/08/23.
//

import Foundation
import Combine

protocol AlbumServiceProtocol {
    func fetchAlbums()-> AnyPublisher<[AlbumModel], Error>
}

struct AlbumService: AlbumServiceProtocol {
    
    var apiHandler: HTTPApiHandlerProtocol
    
    init(apiHandler: HTTPApiHandlerProtocol = HTTPApiHandler()) {
        self.apiHandler = apiHandler
    }
    
    func fetchAlbums()-> AnyPublisher<[AlbumModel], Error> {
        let albumRouter = AlbumEndpoint.albums
        let publishedOutput = apiHandler.fetchData(router: albumRouter, dataType: [AlbumModel].self)
        return publishedOutput.eraseToAnyPublisher()
    }
    
}
