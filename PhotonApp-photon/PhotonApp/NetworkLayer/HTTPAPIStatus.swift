//
//  HTTPAPIStatus.swift
//  WholeApp
//
//  Created by dedeepya reddy salla on 07/08/23.
//

import Foundation

//enum ApiStatus {
//    case apiSuccess
//    case apiFailed(error: Error)
//    case inProgress
//    case apiNotCalled
//
//    var message: String {
//        switch self {
//        case .apiSuccess:
//            return "API received"
//        case .inProgress:
//            return "API call in progress"
//        case .apiNotCalled:
//            return "click on button to get list"
//        case .apiFailed(let error):
//            return "click on button to get list"
//        }
//    }
//}

enum APIStatus {
    case APINotCalled
    case APIReceived
    case APIInProgress
    case apiFailed
}

